/* global $ */
/* global location */
/* global amazon */
/* global URL */

$(document).ready(function() {

  $("#LoginWithAmazon").click(function() {
    var options = { scope : 'profile' };
    amazon.Login.authorize(options, window.location.href);
    return false;
  });
  $('.logout').click(function() { 
    console.log("logout");
    amazon.Login.logout();
    window.location.href = window.location.origin + window.location.pathname;
  });
  
  var url_string = window.location.href;
  var url = new URL(url_string);
  var access_token = url.searchParams.get("access_token");
  
  if (access_token != '' && access_token != null) {
    window.location.href = window.location.origin + window.location.pathname;
    // console.log('redirect');
  }
  
  window.onAmazonLoginReady = function() {
    amazon.Login.setClientId('amzn1.application-oa2-client.6840109fd59340b6b970f96858d586f9');
  
    var options = {
      scope: 'profile',
      interactive: 'never'
    };
    
  
    amazon.Login.authorize(options, function(response) {
      // window.location = window.location.origin + window.location.pathname;
      // console.log('amazon authorize', response);
      if ( response.error ) {
        if (response.error == 'invalid_grant') { // LOGGED OUT
          $(".show-when-logged-out").show();
          $(".show-when-logged-in").hide();
        }
        else {
          console.log('error ' + response.error);
        }
        return;
      }
      amazon.Login.retrieveProfile(response.access_token, function(response) { // LOGGED IN
        // console.log('Hello, ' + response.profile.Name);
        // console.log('Your e-mail address is ' + response.profile.PrimaryEmail);
        // console.log('Your unique ID is ' + response.profile.CustomerId);
        $('.user-name').text(response.profile.Name);
        
        $(".show-when-logged-out").hide();
        $(".show-when-logged-in").show();

                
        // Action : Toggle FAQs
        $(".faq-toggle").click( function () {
          $(".show-faq").toggle();
          $(".faq").toggle();
        });
        
        // Action : Create New Skill
        // Starter
        var starterskill = {"description":"This is a circular Knock Knock joke","name":"Sample Skill","nodes":{"End Joke":{"alexa":["Good Bye","OK Bye"],"nextNodeKey":[""],"title":"End Joke","type":"stop"},"Intro":{"alexa":["Knock knock"],"nextNodeKey":["Tell You"],"reply":{"list":[{"nextNodeKey":["Tell You"],"response":["Who's there","Who is there","Whose there"]}],"type":"custom"},"title":"Intro","type":"decision"},"Loop Joke":{"alexa":["Tell you a joke"],"nextNodeKey":["Intro"],"title":"Loop Joke","type":"transition"},"Tell You":{"alexa":["Tell you"],"reply":{"list":[{"nextNodeKey":["Loop Joke"],"response":["Tell you who"]}],"type":"custom"},"title":"Tell You","type":"decision"}},"startNodeKey":"Intro","stopNodeKey":"End Joke","order":["Intro","Tell You","Loop Joke","End Joke"]};
        $("#skillsjson").val(JSON.stringify(starterskill));
        $('.new-skill').click(function () {
          var input = {
            userID: response.profile.CustomerId,
            skill: JSON.stringify(starterskill)
          };
          $.ajax({
            url: "https://9p3dv0aw86.execute-api.us-east-1.amazonaws.com/prod/AlexaScript/newSkill",
            method: "POST", 
            data: JSON.stringify(input),
            success: function (data, newSkillStatus) {
              window.location.href = '?s=' + data.skillID;
            }
          });
        });
      
        // Action: Select Skill
        var input = {
          userID: response.profile.CustomerId
        };
        $.ajax({
          url: "https://9p3dv0aw86.execute-api.us-east-1.amazonaws.com/prod/AlexaScript/getAllSkills",
          method: "POST", 
          data: JSON.stringify(input),
          success: function (data, getAllSkillsStatus) {
            if (data.status == 'success') {
              var skillID = getUrlParams('s');
              for (var s = 0; s < data.skills.length; s++) {
                var skillOption = $('<option></option>');
                skillOption.attr('value', data.skills[s].skillID);
                skillOption.append(data.skills[s].skillName);
                if (skillID == data.skills[s].skillID) {
                  skillOption.attr('selected', 'selected');
                }
                $(".load-skill-options").append(skillOption);
              }
            }
          }
        });
        
        // Action: Load Skill
        $(".load-skill-button").click(function () {
          skillID = $(this).closest('.load-skill').find('.load-skill-options').val();
          console.log('skillID', skillID);
          window.location.href = '?s=' + skillID;
        });
        var skillID = getUrlParams('s');
        if (skillID != undefined) {
          var input = {
            skillID: skillID
          };
          $.ajax({
            url: "https://9p3dv0aw86.execute-api.us-east-1.amazonaws.com/prod/AlexaScript/getSkill",
            method: "POST", 
            data: JSON.stringify(input),
            success: function (data, getSkillStatus) {
              if (data != null) {
                // console.log("to render", data.skilljson);
                $('.alexa-endpoint-url').val('https://9p3dv0aw86.execute-api.us-east-1.amazonaws.com/prod/AlexaScript/ask/' + skillID)
                render(data.skilljson);
                $('.show-with-valid-skill').show();
        
                // set text areas
                $(".default-utterances").val(
                  'answerIntent {responses}\n' + 
                  'AMAZON.StopIntent end session\n' +
                  'AMAZON.StopIntent cancel\n' +
                  'AMAZON.StopIntent shut up\n' +
                  'AMAZON.StopIntent stop\n' +
                  'AMAZON.StopIntent I am done\n' +
                  'AMAZON.StopIntent I am finished\n' +
                  'AMAZON.HelpIntent help\n' 
                );
                
                // Action : Toggle Amazon Instructions
                $(".amzn-instructions-toggle").click( function () {
                  $(".show-amzn-dev").toggle();
                  $(".amzn-dev-instructions").toggle();
                });
              }
              else {
                $('.show-with-valid-skill').hide();
              }
            }
          });
        }
        else {
          $('.show-faq').hide();
          $('.hide-faq').show();
          $('.faq').show();
        }
        
        // Action: Save Skill
        $(".draw-skill-from-script").click(function() {
          var origScript = $('.script').val()
          var script = origScript.split('\n\n')
          var skillsjson = {
            "name": "",
            "description": "",
            "startNodeKey": "",
            "stopNodeKey": "",
            "nodes": {}
          }
      
          for (var i=0; i<script.length; i++) {
            var lines = script[i].split('\n')
            var currNodeKey = ''
            // console.log('lines', lines)
      
            for (var j=0; j<lines.length; j++) {
              var attributes = lines[j].split(':')
              var value = $.trim(attributes[1]);
              var key = attributes[0];
      
              if (key == "Skill Name") {
                skillsjson.name = value;
              }
              else if (key == "Description") {
                skillsjson.description = value;
              }
              else if (key == "Start") {
                skillsjson.startNodeKey = value;
              }
              else if (key == "Stop") {
                skillsjson.stopNodeKey = value;
              }
              else if (key == "Help") {
                skillsjson.helpNodeKey = value;
              }
              else {
                if (lines[j][0] == "#") {
                  currNodeKey = $.trim(lines[j].replace("#", ""));
                  skillsjson.nodes[currNodeKey] = { title: currNodeKey };
                }
                else {
                  var currNode = skillsjson.nodes[currNodeKey]
                  if (key == "Alexa") {
                    if (value.indexOf("=>") == -1) {
                      currNode.type = "decision"
                      var alexaResponseString = value
                    }
                    else {
                      var alexaResponseString = value.split('=>')[0]
                      var nextNodeKeyString = value.split('=>')[1]
                      if ($.trim(nextNodeKeyString) == 'STOP') {
                        currNode.type = "stop"
                      }
                      else {
                        currNode.type = "transition"
                        currNode.nextNodeKey = nextNodeKeyString.split('||')
                        for (var k=0; k<currNode.nextNodeKey.length; k++){
                          currNode.nextNodeKey[k] = $.trim(currNode.nextNodeKey[k])
                        }
                      }
                    }
      
                    currNode.alexa = alexaResponseString.split('||')
                    for (var k=0; k<currNode.alexa.length; k++){
                      currNode.alexa[k] = $.trim(currNode.alexa[k])
                    }
                  }
                  else if (key == "User") {
                    if (currNode.reply == undefined) {
                      currNode.reply = {
                        type: "custom",
                        list: []
                      }
                    }
                    
                    if (currNode.reply != undefined) {
                      var responseString = value.split('=>')[0]
                      nextNodeKeyString= $.trim(value.split('=>')[1])
                      var nextNodeKey = nextNodeKeyString.split('||')
                      for (var k=0; k<nextNodeKey.length; k++){
                        nextNodeKey[k] = $.trim(nextNodeKey[k])
                      }
      
                      if ($.trim(responseString) == "etc.") {
                        currNode.nextNodeKey = nextNodeKey
                      }
                      else {
                        var response = responseString.split('||')
                        for (var k=0; k<response.length; k++){
                          response[k] = $.trim(response[k])
                        }
      
                        currNode.reply.list.push({
                          response: response,
                          nextNodeKey: nextNodeKey
                        })
                      }
                    }
                  }
                }
              }
      
            }
            script[i] = lines;
          }
          // console.log('script->JSON', skillsjson)
      
          $(".save-ack").show().delay(1000).fadeOut()
      
          // render(skillsjson)
          $(".load-skill-options").find("option[value=" + skillID + "]").text(skillsjson.name)
          var input = {
            // action: 'saveSkill',
            skillID: skillID,
            skillJSON: skillsjson
          };
          // console.log('saveSkill input', input)
          $.ajax({
            url: "https://9p3dv0aw86.execute-api.us-east-1.amazonaws.com/prod/AlexaScript/saveSkill",
            method: "POST", 
            data: JSON.stringify(input),
            success: function (data, saveSkillStatus) {
              render(skillsjson);
            }
          });
        })
        
      });
    });
  };
});


///////////////////////////////// SUPPORTING FUNCCTIONS ///////////////////////////////// 
function getNodeTitleFromKey(nodeKey, skillsjson) {
  if (skillsjson.nodes[nodeKey] == undefined || skillsjson.nodes[nodeKey].title == undefined ) {
    return "ERROR: Not a Node"
  }
  else {
    return skillsjson.nodes[nodeKey].title
  }
}

function render (skillsjson) {
  // sanitize skilljson first
  if (skillsjson.name == undefined || skillsjson.name == null) {
    skillsjson.name = "Untitled"
  }
  if (skillsjson.description == undefined || skillsjson.description == null) {
    skillsjson.description = "Skill needs a description."
  }
  var stopNodeKey = '';
  if (skillsjson.stopNodeKey == undefined || skillsjson.stopNodeKey == null) {
    var lastNodeKey = '';
    for (var n in skillsjson.nodes) {
      if (skillsjson.nodes[n].type == 'stop') {
        var stopNodeKey = n;
        var lastNodeKey = n;
      }
    }
    if (stopNodeKey == '') {
      stopNodeKey = lastNodeKey
    }
    skillsjson.nodes[stopNodeKey].type = 'stop'
  }

  var firstNodeKey = ''
  if (skillsjson.startNodeKey == undefined || skillsjson.startNodeKey == null) {
    var pastFirst = false;
    for (var n in skillsjson.nodes) {
      if (!pastFirst) {
        firstNodeKey = n
      }
      pastFirst = true;
    }
    skillsjson.startNodeKey = firstNodeKey;
  }
  
  var helpNodeKey = ''
  if (skillsjson.helpNodeKey == undefined || skillsjson.helpNodeKey == null) {
    skillsjson.helpNodeKey = skillsjson.startNodeKey;
  }

  for (var n in skillsjson.nodes) {
    var node = skillsjson.nodes[n]
    if (node.title == undefined || node.title == null) {
      node.title = 'Untitled'
    }
    if (node.type == undefined || node.type == null || 
      (node.type != 'decision' && node.type != 'start' && node.type != 'stop' && node.type != 'transition')) {
      node.type = 'decision'
    }
    if (node.alexa == undefined || node.alexa == null) {
      node.alexa = []
    }

    if (node.type == 'decision') {
      if (node.reply == undefined || node.reply == null || node.reply.list == undefined || node.reply.list == null) {
        node.reply = { 'list': [ ] } // { 'response' : '', 'nextNodeKey': '' }
      }
    }
    else if (node.type == 'transition' && (node.nextNodeKey == undefined) || node.nextNodeKey == null) {
      node.nextNodeKey = stopNodeKey;
    }

    if (node.nextNodeKey != undefined && typeof node.nextNodeKey == "string") {
      node.nextNodeKey = [node.nextNodeKey]
    }
  }

  // sanitize utterances & names
  var replaceNodes = []
  for (n in skillsjson.nodes) {
    node = skillsjson.nodes[n]
    // Alexa utterances
    for (var i=0; i<node.alexa.length; i++) {
      node.alexa[i] = sanitizeString(node.alexa[i])
    }
    // User utterances
    if (node.reply != undefined) {
      for (var i=0; i<node.reply.list.length; i++) {
        for (var j=0; j<node.reply.list[i].response.length; j++) {
          node.reply.list[i].response[j] = sanitizeString(node.reply.list[i].response[j])
        }
      }
    }
    // Name/Key
    var newNodeKey = sanitizeString(n)
    newNodeKey = newNodeKey.replaceAll(' ', '')
    replaceNodes.push({'old': n, 'new': newNodeKey})
  }
  // for (var i=0; i<replaceNodes.length; i++) {
  //   skillsjson.nodes[replaceNodes[i].new] = skillsjson.nodes[replaceNodes[i].old]
  //   delete skillsjson.nodes[replaceNodes[i].old]
  // }
  // console.log(skillsjson)

  // sanitize order array
  var displayOrder = skillsjson.order
  var currOrder = []
  var nodeList = []
  for (var n in skillsjson.nodes) {
    currOrder.push(n)
    nodeList.push(n)
  }
  if (skillsjson.order != undefined) {
    for (var i=0; i<displayOrder.length; i++) {
      var key = displayOrder[i]
      currOrder.splice(currOrder.indexOf(key), 1)
      if (nodeList.indexOf(key) == -1) {
        displayOrder.splice(displayOrder.indexOf(key), 1)
      }
    }

    if (currOrder.length != 0) {
      displayOrder = displayOrder.concat(currOrder)
    }
  }
  else {
    displayOrder = currOrder
  }
  skillsjson.order = displayOrder

  // render json textarea
  $("#skillsjson").val(JSON.stringify(skillsjson));

  // render script textarea
  var script = 'Skill Name: ' + skillsjson.name + '\n' +
    'Description: ' + skillsjson.description + '\n' + 
    'Start: ' + skillsjson.startNodeKey + '\n' + 
    'Stop: ' + skillsjson.stopNodeKey + '\n' + 
    'Help: ' + skillsjson.helpNodeKey + '\n';

  for (var j=0; j<displayOrder.length; j++) {
    n = displayOrder[j]
    node = skillsjson.nodes[n]
    script += '\n'
    script += '# ' + n + '\n'

    script += 'Alexa: ' + node.alexa.join(' || ')
    if (node.type == 'transition') {
      script += ' => ' + node.nextNodeKey.join(' || ')
    }
    else if (node.type == 'stop') {
      script += ' => STOP'
    }
    script += '\n'

    if (node.reply != undefined) {
      for (var i=0; i<node.reply.list.length; i++) {
        script += 'User: ' + node.reply.list[i].response.join(' || ') + ' => ' + node.reply.list[i].nextNodeKey.join(' || ') + '\n'
      }
    }
    if (node.type == 'decision' && (node.nextNodeKey != undefined && node.nextNodeKey != 'none')) {
      script += 'User: etc. => ' + node.nextNodeKey.join(' || ') + '\n'
    }
  }
  $(".script").val(script)

  // // set to firebase
  // var user = firebase.auth().currentUser;
  // db.ref().child('users/' + user.uid + '/skills/' + skillID).set(skillsjson)

  var slotResponses = []
  for (n in skillsjson.nodes) {
    node = skillsjson.nodes[n]
    if (node.type == 'decision') {
      for (var i=0; i<node.reply.list.length; i++) {
        for (var j=0; j<node.reply.list[i].response.length; j++) {
          var thisResponse = node.reply.list[i].response[j]
          if (slotResponses.indexOf(thisResponse) == -1) {
            slotResponses.push(thisResponse)
          }
        }
      }
    }
  }
  $("#slot-responses").val(slotResponses.join('\n'));
  $(".node-container").empty();
  $('.node-container').append('<label>Nodes</labels>')
  $(".skill-name").val(skillsjson.name);
  $(".skill-description").val(skillsjson.description);
}

function sanitizeString(str){
  str = str.replace(/[^A-Za-z0-9 \.,']/gim,"");
  return str.trim();
}
String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

function getUrlParams(parameterName) {
  var result = null,
    tmp = [];
  location.search
  .substr(1)
    .split("&")
    .forEach(function (item) {
    tmp = item.split("=");
    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
  });
  return result;
}