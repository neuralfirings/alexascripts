/* global atob */
'use strict';

const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();
const verifier = require('alexa-verifier');

global.navigator = () => null;

exports.handler = (event, context, callback) => {
  console.log("================== Alexa Script v16cd  ================");
  
  // Pre process input params
  var inputGET = {};
  var inputPOST = {};
  var inputURL = {};
  var input = {};
  if (event.queryStringParameters != null) {
    inputGET = event.queryStringParameters;
  }
  if (event.body != null) {
    inputPOST = JSON.parse(event.body);
  }
  if (event.path != undefined && event.path != '/AlexaScript') {
    var paths = event.path.split('/');
    if (paths.length == 4) {
      inputURL[paths[2]] = paths[3];
    }
    else if (paths.length == 3) {
      input.action = paths[2];
    }
  }
  Object.assign(input, inputGET);
  Object.assign(input, inputPOST);
  Object.assign(input, inputURL);
  console.log('Received input:', JSON.stringify(input));

  // Main body of the Lambda Function
  var output = {
    "status": "unknown",
    input: input
  };
  
  
  if (input.ask != undefined && input.ask != null) {
    console.log('Processing an Alexa Utterance');
    var skillID = input.ask;
    
    var paramsForGetSkill = {
      'TableName': 'alexascripts',
      'Key': {
        'skillID': skillID
      }
    };
    
    dynamo.getItem(paramsForGetSkill, function (err, res) {
      if (err) {
        output.status = "error";
        output.method = "getSkill";
        output.error = err;
      }
      else {
        // output.status = "success";
        // output.skilljson = JSON.parse(res.Item.skill);
        var skill = JSON.parse(res.Item.skill);
        console.log("will parse skill", JSON.stringify(skill, null, 2));
        var alexa_output = {
          "version": "1.0",
          "response": {
            "outputSpeech": {
              "type": "PlainText",
              "text": "Spoken response goes here."
            },
            "card": {
              "title": "Skill Title goes here",
              "text": "Skill Card's description goes here",
              "type": "Standard"
            },
            "reprompt": {
              "outputSpeech": {
                "type": "PlainText",
                "text": "Reprompt text goes here"
              }
            },
            "shouldEndSession": false
          },
          "sessionAttributes": {}
        };
        var alexa_input = input;
        
        var preResponse = "";
        var postResponse = "";
  
        console.log('prevNod', getObjValue(alexa_input, "session.attributes.prevNodeKey"));
        if ((alexa_input.request.type === "LaunchRequest" || getObjValue(alexa_input, "session.attributes.prevNodeKey") == undefined) &&
          getObjValue(alexa_input, "request.intent.name") != "AMAZON.HelpIntent") {
          var nextNodeKey = skill.startNodeKey;
          var nextNode = skill.nodes[skill.startNodeKey];
        }
        else if (alexa_input.request.type === "IntentRequest" && alexa_input.request.intent.name == "answerIntent") {
          var prevNodeKey = alexa_input.session.attributes.prevNodeKey;
          var prevNode = alexa_input.session.attributes.prevNode;
          if (prevNode.type == "decision") {
            if (prevNode.reply.type == "custom") {
              var response = alexa_input.request.intent.slots.responses.value
              
              // TODO: need default for currNodeKey
              var foundMatchResponse = false;
              if (response != undefined) {
                for (var i=0; i<prevNode.reply.list.length; i++) {
                  var responseList = prevNode.reply.list[i].response
                  for (var j=0; j<responseList.length; j++) {
                    if (responseList[j].toLowerCase() == response.toLowerCase()) {
                      foundMatchResponse = true;
                      var nextNodeKey = rollTheDice(prevNode.reply.list[i].nextNodeKey);
                    }
                  }
                }
              }
              if (!foundMatchResponse) {
                if (prevNode.nextNodeKey == "none" || prevNode.nextNodeKey == undefined) {
                  var nextNodeKey = prevNodeKey;
                  preResponse = "Hm, I didn't understand. ";
                }
                else {
                  var nextNodeKey = prevNode.nextNodeKey
                }
              }
              var nextNode = skill.nodes[nextNodeKey]; 
            }
            // TODO: else if response reply type is something else, like numbers, dates, etc.
          }
        }
        else if (alexa_input.request.type === "IntentRequest" && alexa_input.request.intent.name == "AMAZON.StopIntent") {
          console.log('stop intent detected');
          var nextNodeKey = skill.stopNodeKey;
          var nextNode = skill.nodes[nextNodeKey];
        }
        else if (alexa_input.request.type === "IntentRequest" && alexa_input.request.intent.name == "AMAZON.HelpIntent") {
          console.log('help intent detected');
          var nextNodeKey = skill.helpNodeKey;
          var nextNode = skill.nodes[nextNodeKey];
        }
        else {
          var nextNodeKey = prevNodeKey;
          var nextNodeKey = prevNode;
        }
  
        var alexaOutputText = ''
        while (nextNode.type == 'transition') {
          alexaOutputText += rollTheDice(nextNode.alexa) + ". "
          prevNodeKey = nextNodeKey
          prevNode = nextNode
          nextNodeKey = rollTheDice(nextNode.nextNodeKey)
          nextNode = skill.nodes[nextNodeKey]
        }
        alexaOutputText += rollTheDice(nextNode.alexa) //nextNode should no longer by transition type
        var outputSpeech = preResponse + " " + alexaOutputText + " " + postResponse;
  
        // Convert Node to Alexa Response
        if (nextNode.type == "stop" || (alexa_input.request.type === "IntentRequest" && alexa_input.request.intent.name == "AMAZON.StopIntent")) {
          alexa_output.response.shouldEndSession = true;    
        }
  
        alexa_output.response.card.title = skill.name;
        alexa_output.response.outputSpeech.text = outputSpeech;
        alexa_output.response.reprompt.outputSpeech.text =outputSpeech;
        alexa_output.response.card.text = outputSpeech;
        alexa_output.sessionAttributes.prevNodeKey = nextNodeKey;
        alexa_output.sessionAttributes.prevNode = nextNode;
      }
      
      alexa_output.sessionAttributes.skillID = skillID;
      
      returnVerifiedOutput(alexa_output, event, function (output) {
        callback(null, output);
      })
    });
  } 
  else if (event.userPoolId === "us-east-1_zfFBE8KEp" && event.triggerSource === "PreSignUp_SignUp") {
    console.log('Auto confirm user sign up');
    event.response.autoConfirmUser = true;
    callback(null, event);
  }
  else if (input.action == 'logIn') {
    var authenticationData = {
      Username : input.email,
      Password : input.password,
    };
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
    
    var userData = {
      Username : input.email,
      Pool : userPool
    };
    
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
        console.log('logged in!');
        // console.log('access token + ' + result.getAccessToken().getJwtToken());
        var idToken = result.getIdToken().getJwtToken();
        // console.log('idToken', idToken);
        var idTokenParts = idToken.split('.');
        var userDataString = new Buffer.from(idTokenParts[1], 'base64').toString('ascii');
        userData = JSON.parse(userDataString);
        console.log('userData', userData);
        // console.log('userID', userData.sub);
        
        var sessionID = new Date().getTime().toString(16) + randomKeyGen(5);
        
        var params = {
          'TableName': 'alexascriptsessions',
          'Item': {
            'sessionID': sessionID,
            'userID': userData.sub,
            'email': userData['cognito:username']
          }
        };
        console.log('dynamo params', params);
        
        dynamo.putItem(params, function (err, res) {
          console.log('put item error', err);
          console.log('put item results', res);
          
          if (err) {
            output.status = "error";
            output.error = err;
          }
          else {
            output.status = "success";
            output.sessionID = sessionID;
            output.email = userData['cognito:username'];
          }
          
          // Callback to generate Lambda's response
          callback(null, returnOutput(output));
          // returnOutput(output, event, function (output) {
          //   callback(null, output);
          // })
        });
      },
      onFailure: function(err) {
        console.log("error", err);
        output.status = "error";
        output.error = err;
        
        // Callback to generate Lambda's response
        callback(null, returnOutput(output));
        // returnOutput(output, event, function (output) {
        //   callback(null, output);
        // })
      }
    });  
  }
  else if (input.action == 'signUp') {
    console.log('signUp');
    
    var attributeList = [];
    var dataEmail = {
      Name : 'email',
      Value : input.email
    };
    var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
    attributeList.push(attributeEmail);
    
    userPool.signUp(input.email, input.password, attributeList, null, function (err, res) {
      if (err) {
        output.status = "error";
        output.method = "signUp";
        output.error = err;
      }
      else {
        output.status = "success";
        output.result = res;
      }
      callback(null, returnOutput(output));
      // returnOutput(output, event, function (output) {
      //   callback(null, output);
      // })
    });
  }
  else if (input.action == 'checkAuth') {
    var paramsForSession = {
      'TableName': 'alexascriptsessions',
      'Key': {
        'sessionID': input.sessionID
      }
    };
    
    if (input.sessionID == undefined || input.sessionID == 'undefined') {
      output.status = "error";
      output.method = "checkAuth";
      output.error = "no session ID";
      callback(null, returnOutput(output));
      // returnOutput(output, event, function (output) {
      //   callback(null, output);
      // })
    }
    else {
      dynamo.getItem(paramsForSession, function (err, res) {
        if (err) {
          output.status = "error";
          output.method = "checkAuth";
          output.error = err;
          callback(null, returnOutput(output));
          // returnOutput(output, event, function (output) {
          //   callback(null, output);
          // })
        }
        else {
          output.status = "success";
          output.email = res.Item.email;
          callback(null, returnOutput(output));
          // returnOutput(output, event, function (output) {
          //   callback(null, output);
          // })
        }
      });
    }
  }
  else if (input.action == 'getAllSkills') {
    console.log('getAllSkills_LWA')
    var userID = input.userID;
    
    var paramsForGetAllSkills = {
      'TableName': 'alexascripts',
      'IndexName': 'userID-skillTimeStamp-index',
      'KeyConditionExpression': 'userID = :userID',
      'ExpressionAttributeValues': {
        ':userID': userID
      },
      'ScanIndexForward': false
    };
    dynamo.query(paramsForGetAllSkills, function (err, res) {
          if (err) {
            output.status = "error";
            output.method = "getAllSkills";
            output.error = err;
            callback(null, returnOutput(output));
          }
          else {
            console.log("results for getAllSkills", res)
            output.status = "success";
            output.skills = [];
            for (var i=0; i < res.Items.length; i++) {
              console.log("this item: ", res.Items[i])
              var skillJSON = JSON.parse(res.Items[i].skill);
              var skill = {
                skillID: res.Items[i].skillID,
                // skillJSON: JSON.parse(res.Items[i].skill)
                skillName: skillJSON.name
              };
              output.skills.push(skill);
            }
            callback(null, returnOutput(output));
            // returnOutput(output, event, function (output) {
            //   callback(null, output);
            // })
          }
        });
  }
  else if (input.action == 'newSkill') {
    console.log('newSkill_LWA')

    var skillTimeStamp = new Date().getTime().toString();
    var userID = input.userID;
    var skillID = Number(skillTimeStamp).toString(36) + randomKeyGen(8);
    
    var paramsForCreateSkill = {
      'TableName': 'alexascripts',
      'Item': {
        'userID': userID,
        'skillID': skillID,
        'skillTimeStamp': skillTimeStamp,
        'skill': input.skill
      }
    };
    console.log('dynamo paramsForCreateSkill', paramsForCreateSkill);
    
    dynamo.putItem(paramsForCreateSkill, function (err, res) {
      console.log('put item error', err);
      console.log('put item results', res);
      
      if (err) {
        output.status = "error";
        output.method = "createSkill";
        output.error = err;
      }
      else {
        output.status = "success";
        output.result = res;
        output.skillID = skillID;
      }
      
      // Callback to generate Lambda's response
      callback(null, returnOutput(output));
      // returnOutput(output, event, function (output) {
      //   callback(null, output);
      // })
    });

  }
  else if (input.action == 'getSkill') {
    console.log('getSkill');
    
    var paramsForGetSkill = {
      'TableName': 'alexascripts',
      'Key': {
        'skillID': input.skillID
      }
    };
    
    dynamo.getItem(paramsForGetSkill, function (err, res) {
      if (err) {
        output.status = "error";
        output.method = "getSkill";
        output.error = err;
      }
      else {
        output.status = "success";
        output.skilljson = JSON.parse(res.Item.skill);
      }
      callback(null, returnOutput(output));
      // returnOutput(output, event, function (output) {
      //   callback(null, output);
      // })
    });
  }
  else if (input.action == 'saveSkill') {
    console.log('saveSkill');
    var paramsForSaveSkill = {
      'TableName': 'alexascripts',
      'Key': {
        'skillID': input.skillID
      },
      // 'skillID': input.skillID,
      'UpdateExpression': 'SET #skill = :skillJSON',
      'ExpressionAttributeNames': { '#skill': 'skill' },
      'ExpressionAttributeValues': { ':skillJSON': JSON.stringify(input.skillJSON) }
    }
    dynamo.updateItem(paramsForSaveSkill, function (err, res) {
      if (err) {
        output.status = "error";
        output.method = "saveSkill";
        output.error = err;
      }
      else {
        output.status = "success";
        output.result = res;
      }
      callback(null, returnOutput(output));
      // returnOutput(output, event, function (output) {
      //   callback(null, output);
      // })
    })
  }
  else {
    output.status = "error"
    output.description = "no matching action param"
    
    callback(null, returnOutput(output));
  }
};

function returnVerifiedOutput (output, event, callback) {
  var obj = {
    "statusCode": 200,
    "headers": {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
    },
    "body": JSON.stringify(output),
  };
  
  console.log("will return output", JSON.stringify(output, null, 2));

  var cert_url = event.headers.SignatureCertChainUrl;
  var signature = event.headers.Signature;
  var requestRawBody = event.body;
  
  verifier(cert_url, signature, requestRawBody, function (err) {
    if (err) {
      obj.status = "error";
      obj.body = JSON.stringify(err); 
      console.log(">>>>>>>>>>>>>>>>>> RETURNOUTPUT: ERROR <<<<<<<<<<<<<<<<<<<")
      console.log(obj)
      callback(obj);
    }
    else {
      console.log(">>>>>>>>>>>>>>>>>> RETURNOUTPUT: GOOD <<<<<<<<<<<<<<<<<<<")
      console.log(obj)
      callback(obj);
    }
  });
}

function returnOutput (output, event, callback) {
  var obj = {
    "statusCode": 200,
    "headers": {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
    },
    "body": JSON.stringify(output),
  };
  
  console.log("will return output", JSON.stringify(output, null, 2));

  return obj;
}

function randomKeyGen(num)
{
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for( var i=0; i < num; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function rollTheDice (options) {
  var optionsNum = Math.floor(Math.random() * options.length);
  return options[optionsNum];
}

function getObjValue (obj, key) {
    return key.split(".").reduce(function(o, x) {
        return (typeof o == "undefined" || o === null) ? o : o[x];
    }, obj);
}