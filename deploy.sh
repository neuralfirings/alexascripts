#!/bin/bash

LAMBDA="yes"
S3="yes"
while [[ $# -gt 1 ]]
do
  key="$1"
  case $key in
      -l|--lambda)
      LAMBDA="$2"
      shift # past argument
      ;;
      -s|--s3)
      S3="$2"
      shift # past argument
      ;;
  esac
  shift # past argument or value
done

if [[ $S3 = "yes" ]] ; then
  aws s3 sync s3 s3://alexascript --delete
fi

if [[ $LAMBDA = "yes" ]] ; then
  rm lambda.zip
  cd lambda/
  zip -X -r ../lambda.zip *
  cd ..
  # zip -r -j lambda.zip ./lambda
  aws lambda update-function-code --function-name AlexaScript --zip-file fileb://lambda.zip
fi